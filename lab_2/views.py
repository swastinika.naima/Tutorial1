from django.shortcuts import render
from lab_1.views import mhs_name, birth_date

#TODO Implement
#Create a content paragraph for your landing page:
landing_page_content = 'Welcome to the landing page that Naima has somehow created. She made this with her blood, sweat, and tears, with no fewer than a dozen contemplations of death along the way. She is so glad that you managed to find her little corner of the internet, and hopes that you enjoy your stay'

def index(request):
    response = {'name': mhs_name, 'content': landing_page_content}
    return render(request, 'index_lab2.html', response)